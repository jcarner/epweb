package seminari1;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servlet
 */
@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DAO dao = null;
		ResultSet res = null;
		String result = "";
		try {
			dao = new DAO();
			res = dao.executeSQL("SELECT * FROM taula");
			
			while(res.next()){
				result += "<tr>"+
								"<td>"+res.getInt("id")+"</td>"
								+"<td>"+res.getString("nom")+"</td>"
								+"<td>"+res.getString("descripcio")+"</td>"
								+"<td>"+res.getInt("telefon")+"</td>"
							+"</tr>";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		PrintWriter out = response.getWriter();
		String title = "HelloWorld! This is my first dynamic web";
		 out.println
		 (ServletUtilities.headWithTitle(title) +
		 "<body>\n" +
		 "<h1>" + title + "</h1>\n" +
		 "<table style='border: 1px solid black;'>"+result+"</table>" );

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
